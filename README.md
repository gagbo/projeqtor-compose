_Acknowledgement_ Thanks to Fabrice Romand <fabrice.romand@gmail.com>
for the [starter](https://gitlab.com/fabrom/projeqtor-docker)

- Tested with Projeqtor 9.3.4

# projeqtor-compose

Dockerized [projeqtor](https://www.projeqtor.org/). The manual is bundled [in the repo](./ProjeQtOrUserGuide_en.pdf)
(useful to look at the API).

The `docker-compose` bundled will build the php image to run Projeqtor,
and uses volumes for the persistent data.

  - [Requirements](#requirements)
  - [Prepare environnement](#prepare-environnement)
  - [Build docker images](#build-docker-images)
  - [Start the stack](#start-the-stack)
  - [Connect to Projeqtor](#connect-to-projeqtor)
  - [First access configuration](#first-access-configuration)
  - [Login to Projeqtor](#login-to-projeqtor)
  - [Stop the stack](#stop-the-stack)
  - [Connect to PHPMyAdmin](#connect-to-phpmyadmin)
  - [Connect to MySQL](#connect-to-mysql)

## Requirements

- Docker
- [Projeqtor package](https://www.projeqtor.org/en/product-en/downloads)

## Prepare environnement

- download and unzip [Projeqtor package](https://www.projeqtor.org/en/product-en/downloads) to *./temp/*
- copy *./temp/projeqtor* to *./data/www/*
- `chmod 777 ./data/www/projeqtor/tool` and `./data/www/projeqtor/files`.
  This is necessary so that the docker image for the application can write 
  necessary configuration to operate

## Build docker images
  - Build projeqtor to [build the php image](./docker/Dockerfile) with the correct plugins

  > $ docker-compose build --force-rm --no-cache --pull --parallel

## Start the stack

  > $ docker-compose up -d

## Connect to Projeqtor

- url: [http://localhost:8080]()

## First access configuration

Replace database host value `127.0.0.1` by `mysql`

Add database password `mysql`

- database host : `mysql`
- database root password : `mysql`
- Save settings, then go to login screen

## Login to Projeqtor
- user: `admin`
- password: `admin`

The first login takes time because of mysql migrations (maybe should move the stack to pg)

## Stop the stack

  > $ docker-compose down -v

## Connect to PHPMyAdmin

- url: [http://localhost:8181]()
- user: *root*
- password: *mysql*

## Connect to MySQL

Use your preferred MySQL client connected to *localhost* port *3306*
(admin user: *root* / password: *mysql*)
